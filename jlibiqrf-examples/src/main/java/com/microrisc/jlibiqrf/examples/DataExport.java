/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microrisc.jlibiqrf.examples;

import static com.microrisc.jlibiqrf.examples.CoordinatorSender.arrayInit;
import static com.microrisc.jlibiqrf.examples.CoordinatorSender.receivedLength;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Calculate index of lost packets, put files together
 * @author Ondrej.Slavicek
 */
public class DataExport {
    
    // Number of tested packets
    private static final int numberOfPackets = 100;
    
    // Arrays store sended time
    private static Long startSmallPackets[] = new Long[numberOfPackets];
    private static Long startMiddlePackets[] = new Long[numberOfPackets];
    private static Long startHugePackets[] = new Long[numberOfPackets];
    
    // Arrays store received time
    private static Long endSmallPackets[] = new Long[numberOfPackets];
    private static Long endMiddlePackets[] = new Long[numberOfPackets];
    private static Long endHugePackets[] = new Long[numberOfPackets];
    
    // Arrays store delay time
    private static Long delaySmallPackets[] = new Long[numberOfPackets];
    private static Long delayMiddlePackets[] = new Long[numberOfPackets];
    private static Long delayHugePackets[] = new Long[numberOfPackets];
    
    private static ArrayList<Long> delaysSmall = new ArrayList<>();
    private static ArrayList<Long> delaysMiddle = new ArrayList<>();
    private static ArrayList<Long> delaysHuge = new ArrayList<>();
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        
        // Array initialization
        arrayInit(startSmallPackets);
        arrayInit(startMiddlePackets);
        arrayInit(startHugePackets);
        arrayInit(endSmallPackets);
        arrayInit(endMiddlePackets);
        arrayInit(endHugePackets);
        arrayInit(delaySmallPackets);
        arrayInit(delayMiddlePackets);
        arrayInit(delayHugePackets);

        // Read files from coordinator
        readCoordinatorFiles("D:\\BP/BPTest/Test_3/Coordinator/Mereni_5/CoordinatorSmallPackets_1.csv", startSmallPackets, endSmallPackets);
        readCoordinatorFiles("D:\\BP/BPTest/Test_3/Coordinator/Mereni_5/CoordinatorMiddlePackets_1.csv", startMiddlePackets, endMiddlePackets);
        readCoordinatorFiles("D:\\BP/BPTest/Test_3/Coordinator/Mereni_5/CoordinatorHugePackets_1.csv", startHugePackets, endHugePackets);

        // Read files from node
        readNodeFiles("D:\\BP/BPTest/Test_3/Node/Mereni_5/NodeSmallPackets_1.csv", delaysSmall);
        readNodeFiles("D:\\BP/BPTest/Test_3/Node/Mereni_5/NodeMiddlePackets_1.csv", delaysMiddle);
        readNodeFiles("D:\\BP/BPTest/Test_3/Node/Mereni_5/NodeHugePackets_1.csv", delaysHuge);

        // Put data together, find right indexes
        connectData(startSmallPackets, endSmallPackets, delaySmallPackets, delaysSmall);
        connectData(startMiddlePackets, endMiddlePackets, delayMiddlePackets, delaysMiddle);
        connectData(startHugePackets, endHugePackets, delayHugePackets, delaysHuge);

        // Generate new files
        generateCsvFile(startSmallPackets, delaySmallPackets, endSmallPackets, "D:\\BP/BPTest/Test_3/W_finishedFiles/Mereni_5/SmallPackets_1.csv");
        generateCsvFile(startMiddlePackets, delayMiddlePackets, endMiddlePackets, "D:\\BP/BPTest/Test_3/W_finishedFiles/Mereni_5/MiddlePackets_1.csv");
        generateCsvFile(startHugePackets, delayHugePackets, endHugePackets, "D:\\BP/BPTest/Test_3/W_finishedFiles/Mereni_5/HugePackets_1.csv");
            
        System.exit(0);
    
    }
    
    /**
     * Put files together, find right indexes for delay times
     * @param arrayStart
     * @param arrayEnd
     * @param arrayDelay
     * @param delays
     */
    public static void connectData(Long arrayStart[], Long arrayEnd[], Long arrayDelay[], ArrayList<Long> delays){
    
        int index = 0;
        
        for (int i = 0; i < numberOfPackets; i++) {
            
            if(index>=delays.size()){break;}
            
            if(i==numberOfPackets-1){
            
                arrayDelay[i] = delays.get(index);
                break;
                
            }
            
            if(arrayStart[i]<delays.get(index) && delays.get(index)<arrayEnd[i]){
            
                arrayDelay[i] = delays.get(index);
                index++;
            
            }else if(arrayStart[i]<delays.get(index) && delays.get(index)<arrayStart[i+1]){
            
                arrayDelay[i] = delays.get(index);
                index++;
                
            }
            
        }
    
    }
    
    /**
     * Read files from node
     * @param fileName
     * @param delays
     */
    public static void readNodeFiles(String fileName, ArrayList<Long> delays){
    
        BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";

	try {

		br = new BufferedReader(new FileReader(fileName));
                br.readLine();
                br.readLine();
		while ((line = br.readLine()) != null) {

		        // use comma as separator
			String[] data = line.split(cvsSplitBy);
                        delays.add(Long.parseLong(data[1]));

		}

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	System.out.println("Node file:" + fileName + " loaded");
    
    }
    
    /**
     * Read files from coordinator
     * @param fileName
     * @param arrayStart
     * @param arrayEnd
     */
    public static void readCoordinatorFiles(String fileName, Long arrayStart[], Long arrayEnd[]){
    
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";

	try {

                int index = 0;
		br = new BufferedReader(new FileReader(fileName));
                br.readLine();
                br.readLine();
                br.readLine();
		while ((line = br.readLine()) != null) {

		        // use comma as separator
			String[] data = line.split(cvsSplitBy);
                        arrayStart[index] = Long.parseLong(data[1]);
                        arrayEnd[index] = Long.parseLong(data[2]);        
                        index++;

		}

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	System.out.println("Coordinator file:" + fileName + " loaded");
  }
    
    
    
    /**
     * Create *.csv file with concrete output
     * Output: Sended packets, xxx
     *         Received packets, xxx
     *         Packet index, Send time, Receive time
     *         xxxxxxxxxxxx, xxxxxxxxx, xxxxxxxxxxx
     * @param startTimes array of sending times
     * @param delayTimes array of delays
     * @param endTimes array of receiving times
     * @param fileName Name of generated file
     */
    public static void generateCsvFile(Long startTimes[], Long delayTimes[], Long endTimes[], String fileName){
        
        try(FileWriter writer = new FileWriter(fileName)){
            
            writer.append("Sended packets");
            writer.append(',');
            writer.append(Integer.toString(startTimes.length));
            writer.append('\n');
            
            writer.append("Coordinator received packets");
            writer.append(',');
            writer.append(receivedLength(endTimes));
            writer.append('\n');
            
            writer.append("Node received packets");
            writer.append(',');
            writer.append(receivedLength(delayTimes));
            writer.append('\n');
            
            writer.append("Packet index");
	    writer.append(',');
	    writer.append("Send time");
            writer.append(',');
	    writer.append("Receive time");
            writer.append(',');
	    writer.append("Delay time");
            writer.append('\n');
            
            
            for (int index = 0; index < numberOfPackets; index++) {
                
                writer.append(Integer.toString(index+1));
                writer.append(',');
                writer.append(Long.toString(startTimes[index]));
                writer.append(',');
                writer.append(Long.toString(endTimes[index]));
                writer.append(',');
                writer.append(Long.toString(delayTimes[index]));
                writer.append('\n');
                
            }
            
	    writer.flush();
            writer.close();
	
        }
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
        
    }
    
}
