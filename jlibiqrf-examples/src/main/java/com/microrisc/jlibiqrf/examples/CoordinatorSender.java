package com.microrisc.jlibiqrf.examples;

import com.microrisc.jlibiqrf.IQRFListener;
import com.microrisc.jlibiqrf.JLibIQRF;
import com.microrisc.jlibiqrf.configuration.IQRFConfiguration;
import com.microrisc.jlibiqrf.iqrfLayer.udp.UDPConfiguration;
import com.microrisc.jlibiqrf.types.IQRFError;
import java.awt.Toolkit;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Coordinator sends data to target node
 * Send test packets
 * @author Ondrej.Slavicek
 */
public class CoordinatorSender implements IQRFListener{
    
    // Scanner for console input
    private static Scanner scan = new Scanner(System.in);
    
    // Number of tested packets
    private static final int numberOfPackets = 100;
    
    // Maximal wait time for response = time slot(50ms) * number of hops(2) * 2(request + response) = 200ms
    private static final int waitTime = 200;//300;
    // Wait time for response => 10 nodes > 1s = 1000ms, 3 nodes > 333ms (iqrf.org)
    private static final int waitTimeFRC = 333;//433;
    
    // Arrays store sended time
    private static Long startSmallPackets[] = new Long[numberOfPackets];
    private static Long startMiddlePackets[] = new Long[numberOfPackets];
    private static Long startHugePackets[] = new Long[numberOfPackets];
    private static Long startIQMeshPackets[] = new Long[numberOfPackets];
    
    // Arrays store received time
    private static Long endSmallPackets[] = new Long[numberOfPackets];
    private static Long endMiddlePackets[] = new Long[numberOfPackets];
    private static Long endHugePackets[] = new Long[numberOfPackets];
    private static Long endIQMeshPackets[] = new Long[numberOfPackets];
    
    // Helpfull ArrayList for unindexed received times
    private static ArrayList<Long> endIQMesh = new ArrayList<>();
    
    //Time value between the current time and midnight, January 1, 1970 UTC(coordinated universal time)
    private static Long currentTime;
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        
        boolean end = false;
        
        // Array initialization
        arrayInit(startSmallPackets);
        arrayInit(startMiddlePackets);
        arrayInit(startHugePackets);
        arrayInit(startIQMeshPackets);
        arrayInit(endSmallPackets);
        arrayInit(endMiddlePackets);
        arrayInit(endHugePackets);
        arrayInit(endIQMeshPackets);
        
        // Create communication layer
        JLibIQRF udpIQRFLayer = createIQRFLayer();
        // Register listener receiving data from network
        udpIQRFLayer.addActionListener(new CoordinatorSender());

        while(true){
        
            switch(scan.next()){

                case "start":
                    smallTest(udpIQRFLayer);
                    middleTest(udpIQRFLayer);
                    hugeTest(udpIQRFLayer);
                    iqMeshTest(udpIQRFLayer);
                    Toolkit.getDefaultToolkit().beep();
                    end = true;
                    break;
                case "small":
                    smallTest(udpIQRFLayer);
                    break;
                case "middle":
                    middleTest(udpIQRFLayer);
                    break;
                case "huge":
                    hugeTest(udpIQRFLayer);
                    break;
                case "test":
                    test(udpIQRFLayer);
                    break;
                case "iqmesh":
                    iqMeshTest(udpIQRFLayer);
                    break;
                case "end":
                    end = true;
                    break; 
                default:
                    System.out.println("Unknown operation");

            }

            if(end){break;}
        
        }

        // Destroy communication layer and free resources
        udpIQRFLayer.destroy();
        
        // Write maesured values to csv file
        generateCsvFile(startSmallPackets, endSmallPackets, "D:\\BP/BPTest/CoordinatorSmallPackets.csv");
        generateCsvFile(startMiddlePackets, endMiddlePackets, "D:\\BP/BPTest/CoordinatorMiddlePackets.csv");
        generateCsvFile(startHugePackets, endHugePackets, "D:\\BP/BPTest/CoordinatorHugePackets.csv");
        assignIndexes(startIQMeshPackets, endIQMesh, endIQMeshPackets);
        generateCsvFile(startIQMeshPackets, endIQMeshPackets, "D:\\BP/BPTest/CoordinatorIQMeshPackets.csv");
        
        System.exit(0);
    
    }
    
    /**
     * Create IQRF communication layer depending on communication type
     * Communication type: UDP
     * localAddress:localPort = 192.168.0.2:55300
     * remoteAddress:remotePort = 192.168.0.254:55000 
     * @return JLibIQRF - IQRF interface for communication
     */
    public static JLibIQRF createIQRFLayer(){
    
        IQRFConfiguration iqrfConfig = new UDPConfiguration("192.168.0.2", 55300, "192.168.0.254", 55000);
        JLibIQRF udpIQRFLayer = JLibIQRF.init(iqrfConfig);
        
        if (udpIQRFLayer == null) {
            System.out.println("ERROR: Creating JLibIQRF");
            System.exit(1);
        }
        
        return udpIQRFLayer;
    
    }
    
    /**
     * Prepared and send simple test packets
     * @param udpIQRFLayer IQRFL communication layer
     */
    public static void test(JLibIQRF udpIQRFLayer){
        
        short[] request = {0x02, 0x00, 0x20, 0x04, 0x01, 0x01, 0x03};
        // Sending read temperature reguest
        int operationResult = udpIQRFLayer.sendData(request);
        // Checking result of operation
        if (operationResult != JLibIQRF.SUCCESS_OPERATION) {
            // If some error has been occured, printing his description
            System.out.println(IQRFError.getDescription(operationResult));
        }
    
    }
    
    /**
     * Prepared and send small packets
     * @param udpIQRFLayer IQRFL communication layer
     */
    public static void smallTest(JLibIQRF udpIQRFLayer){
    
        for (short i = 1; i < numberOfPackets+1; i++) {
            
            // Send DPA request - 1B user data
            short[] request = {0x02, 0x00, 0x20, 0x01, 0x01, 0x01, i};
            startSmallPackets[i-1] = System.currentTimeMillis();
            // Sending reguest
            int operationResult = udpIQRFLayer.sendData(request);
            // Checking result of operation
            if (operationResult != JLibIQRF.SUCCESS_OPERATION) {
                // If some error has been occured, printing his description
                System.out.println(IQRFError.getDescription(operationResult));
            }
            
            // Wait time for response = time slot(50ms) * number of hops(2) * 2(response)
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
            
        }
        
        try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
        }
        
        System.out.println("Small test end!");
        
    }
    
    /**
     * Prepared and send middle packets
     * @param udpIQRFLayer IQRFL communication layer
     */
    public static void middleTest(JLibIQRF udpIQRFLayer){
    
        for (short i = 1; i < numberOfPackets+1; i++) {
            
            // Send DPA request - 24B user data
            short[] request = {0x02, 0x00, 0x20, 0x02, 0x01, 0x01, i, 0x01, 
                                0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 
                                0x0A, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 
                                0x08, 0x09, 0x0A, 0x01, 0x02, 0x03};
            startMiddlePackets[i-1] = System.currentTimeMillis();
            // Sending reguest
            int operationResult = udpIQRFLayer.sendData(request);
            // Checking result of operation
            if (operationResult != JLibIQRF.SUCCESS_OPERATION) {
                // If some error has been occured, printing his description
                System.out.println(IQRFError.getDescription(operationResult));
            }
            
            // Wait time for response = time slot(50ms) * number of hops(2) * 2(response)
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
            
        }
        
        System.out.println("Middle test end!");
        
        try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
        }
        
    }
    
    /**
     * Prepared and send huge packets
     * @param udpIQRFLayer IQRFL communication layer
     */
    public static void hugeTest(JLibIQRF udpIQRFLayer){
    
        for (short i = 1; i < numberOfPackets+1; i++) {
            
            // Send DPA request - 58B user data
            short[] request = {0x02, 0x00, 0x20, 0x03, 0x01, 0x01, i, 0x01, 
                                0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 
                                0x0A, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 
                                0x08, 0x09, 0x0A, 0x01, 0x02, 0x03, 0x04, 0x05, 
                                0x06, 0x07, 0x08, 0x09, 0x0A, 0x01, 0x02, 0x03, 
                                0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x01, 
                                0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 
                                0x0A, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
            startHugePackets[i-1] = System.currentTimeMillis();
            // Sending reguest
            int operationResult = udpIQRFLayer.sendData(request);
            // Checking result of operation
            if (operationResult != JLibIQRF.SUCCESS_OPERATION) {
                // If some error has been occured, printing his description
                System.out.println(IQRFError.getDescription(operationResult));
            }
            
            // Wait time for response = time slot(50ms) * number of hops(2) * 2(response)
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
            
        }
        
        try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
        }
        
        System.out.println("Huge test end!");
        
    }
    
    /**
     * Prepared and send IQMesh temperature packets
     * @param udpIQRFLayer IQRFL communication layer
     */
    private static void iqMeshTest(JLibIQRF udpIQRFLayer) {

        for (short i = 1; i < numberOfPackets+1; i++) {
            
            // Send DPA read temperature request - FRC
            short[] temperatureRequest = {0x00, 0x00, 0x0D, 0x00, 0xFF, 0xFF, 0x80, 0x00, 0x00};
            startIQMeshPackets[i-1] = System.currentTimeMillis();
            // Sending read temperature reguest
            int operationResult = udpIQRFLayer.sendData(temperatureRequest);
            // Checking result of operation
            if (operationResult != JLibIQRF.SUCCESS_OPERATION) {
                // If some error has been occured, printing his description
                System.out.println(IQRFError.getDescription(operationResult));
            }
            
            // Wait time for response => 10 nodes > 1s = 1000ms, 3 nodes > 333ms (iqrf.org)
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
            
        }
        
        try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                System.out.println(ex);
        }
        
        System.out.println("IQMESH test end");

    }
    
    /**
     * Process received data, identify test packets
     * This method is called after each receiving data
     * @param data received from network
     */
    public static void dataHandler(short[] data){
        
        if(data[2]==32){
        
            System.out.println("Tested packet");
            
            switch(data[3]){
            
                case 129:
                    currentTime = System.currentTimeMillis();
                    System.out.println("TimeCMD1: " + currentTime);
                    endSmallPackets[data[8] - 1] = currentTime;
                    break;
                case 130:    
                    currentTime = System.currentTimeMillis();
                    System.out.println("TimeCMD2: " + currentTime);
                    endMiddlePackets[data[8] - 1] = currentTime;
                    break;
                case 131:
                    currentTime = System.currentTimeMillis();
                    System.out.println("TimeCMD3: " + currentTime);
                    endHugePackets[data[8] - 1] = currentTime;
                    break;
                case 132:
                    System.out.println("Connection test - SUCCESSFUL");
                    break;
                default:
                    System.out.println("Confirmation");
                    
            } 
        
        }else{
            
            if(data[2]==13 && data[3]==128){
                currentTime = System.currentTimeMillis();
                System.out.println("TimeIQMesh: " + currentTime);
                endIQMesh.add(currentTime);
            }else{
        
                System.out.println("Normal packet.");
            
            }
        } 
    
    }

    /**
     * Calculate length of received packets array
     * @param endTimes
     * @return length of array
     */
    public static String receivedLength(Long endTimes[]) {
        
        int length = 0;
        
        for (Long endTime : endTimes) {
            
            if(endTime!=0){
            
                length++;
            
            }
            
        }
        
        return Integer.toString(length);
    
    }

    /**
     * Assign indexes of received time - IQMesh packets
     * @param startIQMeshPackets array of sending times
     * @param endIQMesh ArrayList of receiving times
     * @param endIQMeshPackets array of receiving times
     */
    private static void assignIndexes(Long[] startIQMeshPackets ,ArrayList<Long> endIQMesh, Long[] endIQMeshPackets) {
        
        int index = 0;
        
        for (int i = 0; i < numberOfPackets; i++) {
            
            if(index >= endIQMesh.size()){break;}
            
            if(i==numberOfPackets-1){
            
                endIQMeshPackets[i] = endIQMesh.get(index);
                break;
                
            }
            
            if(startIQMeshPackets[i]<endIQMesh.get(index) && endIQMesh.get(index)<startIQMeshPackets[i + 1]){
            
                endIQMeshPackets[i] = endIQMesh.get(index);
                index++;
            
            }
            
        }
        
    }
    
    @Override
    public void onGetIQRFData(short[] data) {
        
        System.out.println("Received data: " + Arrays.toString(data));
        
        if(data!=null){
            
            dataHandler(data);
        
        }
    
    }
    
    /**
     * Create *.csv file with concrete output
     * Output: Sended packets, xxx
     *         Received packets, xxx
     *         Packet index, Send time, Receive time
     *         xxxxxxxxxxxx, xxxxxxxxx, xxxxxxxxxxx
     * @param startTimes array of sending times
     * @param endTimes array of receiving times
     * @param fileName Name of generated file
     */
    public static void generateCsvFile(Long startTimes[], Long endTimes[], String fileName){
        
        try(FileWriter writer = new FileWriter(fileName)){
            
            writer.append("Sended packets");
            writer.append(',');
            writer.append(Integer.toString(startTimes.length));
            writer.append('\n');
            
            writer.append("Received packets");
            writer.append(',');
            writer.append(receivedLength(endTimes));
            writer.append('\n');
            
            writer.append("Packet index");
	    writer.append(',');
	    writer.append("Send time");
            writer.append(',');
	    writer.append("Receive time");
	    writer.append('\n');
            
            
            for (int index = 0; index < numberOfPackets; index++) {
                
                writer.append(Integer.toString(index+1));
                writer.append(',');
                writer.append(Long.toString(startTimes[index]));
                writer.append(',');
                writer.append(Long.toString(endTimes[index]));
                writer.append('\n');
                
            }
            
	    writer.flush();
            writer.close();
	
        }
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
        
    }
    
    /**
     * Initialization all members of array to zero
     * @param array for initialization
     */
    public static void arrayInit(Long array[]){
    
        for (int i = 0; i < array.length; i++) {
            
            array[i] = new Long(0);
            
        }
    
    }
    
}
