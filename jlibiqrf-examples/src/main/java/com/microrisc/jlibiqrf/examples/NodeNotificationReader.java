package com.microrisc.jlibiqrf.examples;

import com.microrisc.jlibiqrf.IQRFListener;
import com.microrisc.jlibiqrf.JLibIQRF;
import com.microrisc.jlibiqrf.configuration.IQRFConfiguration;
import com.microrisc.jlibiqrf.iqrfLayer.serial.SerialConfiguration;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Reading notifications received from node and processing test packets
 * Node send notification about received packet via UART
 * @author Ondrej.Slavicek
 */
public class NodeNotificationReader implements IQRFListener{
    
    // Scanner for console input
    private static Scanner scan = new Scanner(System.in);
    // ArrayList store receved time
    private static ArrayList<Long> smallPackets = new ArrayList<>();
    private static ArrayList<Long> middlePackets = new ArrayList<>();
    private static ArrayList<Long> hugePackets = new ArrayList<>();
    
    //Time value between the current time and midnight, January 1, 1970 UTC(coordinated universal time)
    private static Long currentTime;
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        
        // Create communication layer
        JLibIQRF serialIQRFLayer = createIQRFLayer();
        // Register listener receiving data from network
        serialIQRFLayer.addActionListener(new NodeNotificationReader());

        // End of communication
        while(!"end".equals(scan.next()));
        
        // Destroy communication layer and free resources
        serialIQRFLayer.destroy();
        
        // Write maesured values to csv file
        generateCsvFile(smallPackets, "D:\\BP/BPTest/NodeSmallPackets.csv");
        generateCsvFile(middlePackets, "D:\\BP/BPTest/NodeMiddlePackets.csv");
        generateCsvFile(hugePackets, "D:\\BP/BPTest/NodeHugePackets.csv");

        System.exit(0);
    
    }
    
    /**
     * Create IQRF communication layer depending on communication type
     * Communication type: UART on COM port 5, baud rate 9600Bd 
     * @return JLibIQRF - IQRF interface for communication
     */
    public static JLibIQRF createIQRFLayer(){
    
        IQRFConfiguration iqrfConfig = new SerialConfiguration("COM5", 9600);
        JLibIQRF serialIQRFLayer = JLibIQRF.init(iqrfConfig);
        
        if (serialIQRFLayer == null) {
            System.out.println("ERROR: Creating JLibIQRF");
            System.exit(1);
        }
        
        return serialIQRFLayer;
    
    }
    
    /**
     * Process received data, identify test packets
     * This method is called after each receiving data
     * @param data received from network
     */
    public static void dataHandler(short[] data){
    
        if(data[2]==32){
        
            System.out.println("Tested packet");
            
            switch(data[3]){
            
                case 1:
                    currentTime = System.currentTimeMillis();
                    System.out.println("TimeCMD1: " + currentTime);
                    smallPackets.add(currentTime);
                    break;
                case 2:    
                    currentTime = System.currentTimeMillis();
                    System.out.println("TimeCMD2: " + currentTime);
                    middlePackets.add(currentTime);
                    break;
                case 3:
                    currentTime = System.currentTimeMillis();
                    System.out.println("TimeCMD3: " + currentTime);
                    hugePackets.add(currentTime);
                    break;
                case 4:
                    System.out.println("Connection test");
                    break;
                default:
                    System.out.println("ERROR: Unknown CMD!");
                    
            } 
        
        }else{
        
            System.out.println("Normal packet.");
        
        }    
    
    }
    
    @Override
    public void onGetIQRFData(short[] data) {
        
        System.out.println("Received data: " + Arrays.toString(data));
        
        dataHandler(data);
        
    }
    
    /**
     * Create *.csv file with concrete output
     * Output: Received packets, xxx
     *         Packet index, Receive time
     *         xxxxxxxxxxxx, xxxxxxxxxxxx
     * @param times ArrayList of measured times
     * @param fileName Name of generated file
     */
    public static void generateCsvFile(ArrayList<Long> times, String fileName){
	
        if(times.isEmpty()){return;}
        
        try(FileWriter writer = new FileWriter(fileName)){
            
            writer.append("Received packets");
            writer.append(',');
            writer.append(Integer.toString(times.size()));
            writer.append('\n');
            
            writer.append("Packet index");
	    writer.append(',');
	    writer.append("Receive time");
	    writer.append('\n');
            
            for (Long time : times) {
                
                writer.append(Integer.toString(times.indexOf(time)+1));
                writer.append(',');
                writer.append(time.toString());
                writer.append('\n');
                
            }
            
	    writer.flush();
            writer.close();
            
	}
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
    }
    
}
